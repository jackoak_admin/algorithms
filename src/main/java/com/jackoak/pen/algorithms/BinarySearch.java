package com.jackoak.pen.algorithms;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;

public class BinarySearch {

    public static int rank(int key,int[] a){
        if(a == null){
            throw new NullPointerException();
        }

        int lo = 0;
        int hi = a.length - 1;

        while(lo <= hi){
            int mid = lo + (hi - lo)/2;
            if(key > a[mid]){
                lo = mid + 1;
            }else if(key < a[mid]){
                hi = mid - 1;
            }else{
                return mid;
            }
        }

        return -1;
    }

    public static void main(String[] args){
        In in = new In(args[0]);
        int[]  whilteList = in.readAllInts();
        Arrays.sort(whilteList);

        while(!StdIn.isEmpty()){
            int key = StdIn.readInt();
            if(rank(key,whilteList) < 0){
                StdOut.println("该数不在白名单里");
            }else{
                StdOut.println("该数 在白名单里");
            }
        }

    }

}
