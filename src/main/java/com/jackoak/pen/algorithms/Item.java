package com.jackoak.pen.algorithms;

public class Item {
    private int age;
    private String address;

    public Item(int age,String address){
        this.age = age;
        this.address = address;
    }



    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Item{" +
                "age=" + age +
                ", address='" + address + '\'' +
                '}';
    }
}
