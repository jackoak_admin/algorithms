package com.jackoak.pen.algorithms;

import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;

import java.util.Arrays;

public class StaticsETTofInts {
    private int[] a;

    public StaticsETTofInts(int[] keys){
        a = new int[keys.length];
        for(int i = 0; i < keys.length; i++){
            a[i] = keys[i]; // 保护性复制
        }
        Arrays.sort(a);
    }

    public boolean contains(int key){
        return rank(key) != -1;
    }

    private int rank(int key){
        int lo = 0;
        int hi = a.length - 1;
        while(lo <= hi){
            int mid = lo + (hi - lo)/2;
            if(key > mid){
                lo = mid + 1;
            }else if(key < mid){
                hi = mid -1;
            }else{
                return mid;
            }
        }

        return -1;
    }

    public static  void main(String[] args){
        In in = new In(args[0]);
        int[] w = in.readAllInts();
        StaticsETTofInts set = new StaticsETTofInts(w);
        while (!StdIn.isEmpty()){
            int key = StdIn.readInt();
            if(set.contains(key)){
                StdOut.println("白名单包含: " + key);
            }else{
                StdOut.println("不包含");
            }
        }
    }

}
