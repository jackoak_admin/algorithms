package com.jackoak.pen.algorithms;

import edu.princeton.cs.algs4.Bag;
import edu.princeton.cs.algs4.StdOut;

public class BagTest {

    public static void main(String[] args)throws Exception{
        Bag<Item> bag = new Bag<>();
        Item item1 = new Item(25,"北京育知路");
        Item item2 = new Item(21,"重庆市大足区");
        Item item3 = new Item(221,"济南高新区");
        Item item4 = new Item(234,"哈尔滨理工大学");

        bag.add(item1);
        bag.add(item2);
        bag.add(item3);
        bag.add(item4);
        for(Item itemFor : bag){
            StdOut.println(itemFor);
        }

    }

}
